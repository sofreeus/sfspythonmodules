dunderLAB
---------

Q - "How does he program or script know it is the 'main',
    or top-level program? (What is the program's 'scope')"

    All python source files end .py
    That could be a program, script or module.

Q - "What if you intend for a module to only be imported
    by another 'main' program, and never for the module
    to be the main program?"

Q - "How does this affect how the main program or the
     imported module program will execute"

1) CREATE DUNDER1 FILE; EXECUTE
 vi dunder1.py
 def testfunc():
     return
 print("this is dunder1.py module name", __name__)
 print("this is dunder1.py module file path", __file__)

run
 python dunder1.py


2) CREATE DUNDER2 FILE WITH DUNDER1 IMPORTED; EXECUTE
 vi dunder2.py
 import dunder1

run
 python dunder2.py

** NOTE THE DIFFERENCE IN THE OUTPUT WHEN IT'S AN **
** IMPORTED MODULE VS. BEING RUN AS THE "MAIN" PROGRAM **
** (__main__ is actully a sort of hidden module)  **


3) ADD THE PRINT STATEMENT TO DUNDER2; EXECUTE
 vi dunder2.py
> print("this is dunder2.py module name", __name__)
> print("this is dunder2.py module file path", __file__)

** NOTE THE DIFFERENCE BETWEEN DUNDER1 AND DUNDER2 **
** WITH THE 'MAGIC' VARIABLES **
** THE FILE VARIABLE NOW SHOWS THE SOURCE OF THE **
** IMPORTED MODULE, AND IT'S NAME **

4) FIND THE VAR TYPE IN DUNDER1
 print("__file__ variable is of type:", type(__file__))


5) ADD SOME REAL CODE TO THE FUNCTION IN DUNDER1 &
   DUNDER2 AGAIN


5) REDO DUNDER2 AS IMPORT testfunc from dunder1

  ** NOTE THAT YOU CAN IMPORT IF THE MOD IS IN YOUR PATH **
  ** OTHERWISE, YOU NEED SOME SPECIAL KIND OF MODULE CALLED **
  ** A PCKAGE, THAT HAS SPECIAL DUNDER ATTRIBUTED TO **
  ** TELL IT THE LOCATION FOR PACKAGES **


6) DO THE SAME IN iPython AND SEE THE AUTOCOMPLETE
  ipython

  from dunder1 import <TAB>
  ** NOTE THAT THERE WAS OUTPUT FROM FUNCTION **
  ** DO IT AGAIN, AND NOTICE THAT THERE IS NONE **

7) EXTRA: IMPORT as AN ALIAS

7) EDIT DUNDER1 AND GIVE THE FUNCTION A POSITIONAL ARG



8) APPEND if '__main__' == __name__ AND SEE OUTPUT DIFFERENCE

9) TO READ MORE ABOUT THIS
   https://docs.python.org/3/library/__main__.html
