Demo 1 in repl.it (30min)
-----

5   <= _first python program. Notice no error or failure._

-----

x = 5

-----

5 + 5

-----

x = 5 + 5

-----

x = 5 pl 5  <= _sometimes trial and error can work in Python_

-----
```
x = 5
print(x)
```
-----
```
x = 5 + 5
print (x)
```


_Chat about operator overloading (like + concat)_

_talk about ==_

~~~
    print(Badger)
    Badger = �honey�
    print(Badger)
    ```
    * Variable Name Rules:
      1. Names can't start with a number.
        * They must start with a `_` or a letter.
        * Variables or functions that start with `_` are typically used to denote internal variables
      2. Names can only use letters, numbers, and `_`.
        * `::SaltineCracker::` or `Dip&Dot` are not valid names
      3. Names are case-sensitive
        * `foo`, `Foo`, and `FOO` are treated as three separate variables

  * Printing things to the console:
~~~

```
> print('Hello' + 'World!')
Hello World!
```
    
-----


print(5 + 5)


------

print  (5 + 5)

------

print(5+5)    <= _Do all 3 of these 'prints' behave the same? Does space matter within a line?_

-------
_Does space matter at the begining of a line?_

      print (5+5)  

-------

_typical non-python way to define code bock. Leading white-space defines code in Py_   
  



```  

def psydo_code {

bunch of code to    
do things

}
```

-------

Google python PEPs for this type of information

------

import this  <= _show Zen of Python_

------

~~~
* Indentation
    * 4 spaces shows that the code is inside a block, such as an if statement
~~~

-------
clear repl.it
-------
```
x = 5 + 5  
print(x)
```
-------
```
def func():
    x = 5 + 5
    print(x)
```
-------
```
def func():
    x = 5 + 5
    print(x)
func()
```
-------
```
def fplf():
  5 + 5 
fplf()
```
-------
```
def fplf(n):
  n + 5 
fplf(5)
```
-------
```
def fplf(n):
  return n + 5
fplf(5)
```
-------
```
def fplf(n):
  return n + 5
print(fplf(5))

```
-------
```
def fplf(n):
  print(n + 5) 
fplf(5)
```
-------
```
def fplf(n):
  print("calculating a var + the number 5...", n + 5) 
fplf(5)
```
------

```
def fplf(n, x):
  print("calculating a var + the number 5...", n + 5)
fplf(5, "dfdsfdsfsd")

```

-------


_talk about ==_

```
x = 5
if x == 5:
    print("I guess this is five")
```

-------
__re-iterate__  
_- Explain that print in a function in P3, and functions take arguments in parenth  
- Talk : to start code block  
- Talk white space (space v TAB)  
- Talk PEP 4 space std  
- Talk that def func assume a code block, so you need :
  but you can just "pass" as code block (implied "return")_  

  
--------
