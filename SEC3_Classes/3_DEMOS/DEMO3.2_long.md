LAB 3
------
```
def function(n):
    n + " World"

```
__ import and call function in REPL and see no output.
give it bad data, such as an int, and see error output.
give it no argument, and see error output __
------
```
def function(n):
    return n + " World"

```
__ run both in REPL and in .py file __

__'return' terminates the execution of the function, and returns
data to calling function_
in a REPL, it also acts like 'print', since P is part of REPL.
You can see the return to the function in REPL,
but can't see what's going on inside the .py file
If you want to see the return value, wrap it in a print__

------

```
def function(n):
    print(n + " World")

```

------

```
def function(n):
    print(n, "World")

```

------