LAB 2
-----

---------------------------

LAB2.1

   Write a program in a text editor, that has a function that's called within the program. 
   This function will evaluate 1 positional argument.
   It will check whether the argument lower-case matches the first name of either neighbor.
   If it does, it will print thier name, followed by the string "is my neighbor".
   If it does not, it will print out the argument input, followed by the string "is not my neighbor"
   No matter what, this program will always print "David Willson is a llama"

   This program should be written such that it can be called by the Python3 interpreter on the OS command line 

   Run your program and note the output.

LAB2.2

   In the same direcotry you saved your LAB2.1 program, drop into installed Python3 REPL sub-prompt (>>>), and import the LAB2.1 module.  
   Explain what happens.
   Now import it again, and explain what happens.
   Do a ``` dir(module_name)``` 
   Exit the REPL with the quit function call


LAB2.3
   Enter the REPL again, and import the LAB2.1 module.
   Create an instance or "object" of that module's method <-- _new words!_
   Pass it an argument <-- _How did you know what were valid arguments? String or int? # of arg?_  

LAB2.4

   Again, go into the REPL in your shell.
   Type on the first line 'x = 5 + 5'
   On the second line, type 'x'

   Now open repl.it/languages/python3
   Type on the first line 'x = 5 + 5'
   On the second line, type 'x'

   Did the REPLs behave any differently?

LAB2.5 (extra)
   Install an IDE like Spyder or PyCharm, snd repeat 2.3-2.4. Play with hover-over, <TAB><TAB>, and auto-complete

LAB2.6 (extra)
   Play around with ```return``` in a function, and behaviour between REPL and .py file
  
ex.

```
def function(n):
    n + " World"

```
__ import and call function in REPL and see no output.
give it bad data, such as an int, and see error output.
give it no argument, and see error output __
------
```
def function(n):
    return n + " World"

```
__ run both in REPL and in .py file __

__'return' terminates the execution of the function, and returns
data to calling function_
in a REPL, it also acts like 'print', since P is part of REPL.
You can see the return to the function in REPL,
but can't see what's going on inside the .py file
If you want to see the return value, wrap it in a print__

------

```
def function(n):
    print(n + " World")

```

------

```
def function(n):
    print(n, "World")

```

------
