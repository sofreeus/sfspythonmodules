* Mathematical operators:
  * Binary Operators:
    * `+`, `-`, `*`, `/` are add, subtract, multiply, divide
    * `%` is the modulus operator. It gets the remainder of dividing one number on another, i.e. `10 % 3` = 1, because `10 / 3` = 3 with a remainder of 1.
    * `var += 3` is the same as `var = var + 3`, and this works with all other binary operators

  * Conditional operators:
    * `==`, `>`, `<`, `<=`, `>=`
    * `==` asks if the two values are equal while `=` tells that the two values are equal.
  * Logical operators:
    * `and`, `or`, `not`
    * `x + y == 100 and x % 2 == 0`
* If statements
  * Only executes a piece of code if the condition is true
  ```
  > foo = 3
  > if foo == 3:
  >     print('foo is equal to 3')
  > elif foo == 5:
  >     print('foo is equal to 5')
  > else:
  >     print('foo is neither 3 nor 5')
  foo is equal to 3
  ```
* Loops
  * While loop
  ```
  > num = 5
  > while num < 10:
  >     print(num)
  >     num = num + 1
  5
  6
  7
  8
  9
  ```
* Input
  * Use `input()` to ask for input from the terminal
  ```
  > name = input('Type a name for me: ')
  > print('You typed "{}"'.format(name))
  Type a name for me: Zoe
  Zoe
  ```
  * `input()` takes in input in the form of a string, so if you're looking for a number, you need to force it to be a number so you can do operations on it:
  ```
  > num = int(input('Type a number: '))
  > num -= 1
  > print(num)
  Type a number: 8
  7
  ```


## Demo: A New Euler
While loop:
```
i = 0
while i < 10:
    print(i)
    i += 1
```
if statement:
```
if i - 1 == 3:
    print(i)
elif i - 2 == 3:
    print(i)
```
Modulus operator:
```
10 % 3 == 1
```
