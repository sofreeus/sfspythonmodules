## CLASS

_Difference between the CLASS itself, and the CLASS called in a program (sks CLASS object):_

__CLASS FUNCTION__
From a CLASS perspective, an ATTRIBUTE (think property) of that CLASS is usually a FUNCTION within the CLASS.

__CLASS METHOD__
From a program perspective, that CLASS ATTRIBUTE is called a METHOD.
"A method is a function that 'belongs to' an object (eg a CLASS object)"


-------

 Classes, Functions, & Imports
* Functions
  * Declaring a function:
    * No need to declare the type of the variables
    * Variables can have a default value
    * function names are usually lowercase
  ```
  > def disseminate(information='Informative nonsense!'):
  >     """
  >     Considerate to have information on the function and what it does
  >     in a multiline comment here, unless it's quite obvious and/or simple.
  >     """
  >     information = information.strip()
  >     print(information)
  ```
  * Calling a function:
    * Calling the function using the default value
    * Calling the function using a named parameter
    * Calling the function using a positional parameter
  ```
  > disseminate()
  > disseminate('The second longest word is hippopotomonstrosesquippedaliophobia.')
  > disseminate(information='Hippopotomonstrosesquippedaliophobia is the fear of long words.')
  'Informative nonsense!'
  'The second longest word is hippopotomonstrosesquippedaliophobia.'
  'Hippopotomonstrosesquippedaliophobia is the fear of long words.'
  ```
* Classes
  * Classes are objects. They package data and functions for more convenient use.
  * Class names usually follow CamelCase.
  * Classes must have an \_\_init\_\_ function.
    * This function is called when the class is created.
    * It declares the variables carried by the class.
  * `self` is the keyword used to refer to the class the code is currently in.
  * Declaring a class:
  `crier.py`
  ```
  > class TownCrier:
  >     def __init__(self, town_name, volume=11):
  >         self.town = town_name
  >         self.volume = volume
  >
  >     def shout(self, news):
  >         self.volume += 3
  >         disseminate(news)
  ```
  * Using a class:
  ```
  > Reggie = TownCrier('New York')
  > Ruby = TownCrier('New York', 13)
  > Reggie.shout('Extra!')
  > Ruby.shout('Bigger Extra! Don't listen to Reggie!)
  ```
* Importing
  * Importing functions and classes is a fantastic way to organize large bodies of code as well as to use outside libraries.
  * Importing outside modules:
  ```
  > import time
  > print(time.time())
  1533368997.8783445
  ```
  * Importing local modules:
  ```
  > from crier import TownCrier
  >
  > Reggie = TownCrier('New York')
  > Ruby = TownCrier('New York', 13)
  > Reggie.shout('Extra!')
  > Ruby.shout('Bigger Extra! Don't listen to Reggie!)
  ```

## Demo:
`main.py`
```
> from greeter import greeter
> greeter('World').greet()
```
`greeter.py`
```
class greeter:
    def __init__(self, name):
        self.name = name

    def greet(self):
        print('Hello ' + self.name + '!')
```
