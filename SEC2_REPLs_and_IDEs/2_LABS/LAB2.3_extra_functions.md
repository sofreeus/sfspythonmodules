-----------

Lab 2 

1)

vi file1.py
```
print("hello world")
```
:wq!

```
python file1.py
```
(returns output)


2)

edit file1.py
```
> def hellofunc():
print("hello world")
```
```
python file1.py
```
(loads function but no output)


3)

_import file1.py 'source file module', into file2.py_

vi file2.py
```import file1``` (note, no .py extension)(note: to import, they need to be in the py path)

_run file2.py_

```python file2.py``` (note: module loads, but does nothing)


4)

vi file2.py
_edit_ one line to say
from file1 import hellofunc()

_run file2.py_
```python file2.py``` (note: function from module loads, but does nothing)


5)

_Now use the imported function_

vi file2.py
add second line to call the imported function
```hellofunc()```  (the code block of the function is "hello world", so calling it will do exactly that)


6) 

_Do something else with the function, like assign it a var (make it an object)?_

7) 

_import file1 into a REPL and then into a IDE, and see that I can know what the module's function is_

8) 

_Use the dir(module) in the file2.py script to show how to get that information._

9) 

_Edit the file1.py and instead of (), give it an assignment/argument, in Spyder (see hoverover)_

10) 

_give it 'x' as argument, but nothing when you call it, and see the error_

11) 

_change "x" to something more meaningful, and see it in the hoverover_

11) 

_repeat same function / module test on a built-in one from std lib_
